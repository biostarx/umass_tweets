UMass Tweets - Data Exploration and Sentiment Analysis
================
William
2017-10-22

Load packages
-------------

``` r
library('rtweet'); packageVersion('rtweet')
```

    ## Welcome to rtweet v0.4.0!

    ## [1] '0.4.0'

``` r
library('wordcloud'); packageVersion('wordcloud')
```

    ## Loading required package: RColorBrewer

    ## [1] '2.5'

``` r
library('tm'); packageVersion('tm')
```

    ## Loading required package: NLP

    ## [1] '0.7.1'

``` r
library('SnowballC'); packageVersion('SnowballC')
```

    ## [1] '0.5.1'

``` r
library('SentimentAnalysis'); packageVersion('SentimentAnalysis')
```

    ## 
    ## Attaching package: 'SentimentAnalysis'

    ## The following object is masked from 'package:base':
    ## 
    ##     write

    ## [1] '1.2.0'

``` r
library('ggplot2'); packageVersion('ggplot2')
```

    ## 
    ## Attaching package: 'ggplot2'

    ## The following object is masked from 'package:NLP':
    ## 
    ##     annotate

    ## [1] '2.2.1'

``` r
library('ggmap'); packageVersion('ggmap')
```

    ## [1] '2.6.1'

``` r
library('maps'); packageVersion('maps')
```

    ## [1] '3.2.0'

Description
-----------

Here, I used the Twitter plataform for a sentiment analysis. The R package rtweet was used to retrieve public tweets with the "UMass" word. This yielded 12,451 tweets. For reproducibility purposes, tweets were saved to an R object called UMass\_rt.Robj.

``` r
rt <-search_tweets("UMass", token = twitter_token, n = 100000, retryonratelimit = TRUE)
```

You can skip the previous step and instead load the data from the UMass\_rt.Robj as follow.

``` r
load('UMass_rt.Robj')
```

``` r
rt.data <- tweets_data(rt)
```

Data Exploration
----------------

How many users?

``` r
length(unique(rt.data))
```

    ## [1] 35

What is time range?

``` r
range(rt.data$created_at)
```

    ## [1] "2017-10-11 08:33:56 UTC" "2017-10-21 06:47:08 UTC"

From what countries tweets were made?

Note, this type of information is limited. Only a small fraction of tweets has the location. However, for the sentiment analysis the whole dataset will be used.

``` r
df = as.data.frame(table(rt.data$country))
colnames(df) = c('region','frequency')
df$region = as.character(df$region)
df
```

    ##                 region frequency
    ## 1               Brazil         4
    ## 2               Canada         2
    ## 3       Czech Republic         1
    ## 4               France         1
    ## 5               Greece         2
    ## 6              Hungary         1
    ## 7                Japan         1
    ## 8  Trinidad and Tobago         1
    ## 9       United Kingdom         2
    ## 10       United States       334

This many tweets contain the location metadata follow by the percentage the fraction represent from the total.

``` r
sum(df$frequency);paste(round((sum(df$frequency)/nrow(rt.data))*100,digits = 2),sep='','%')
```

    ## [1] 349

    ## [1] "2.8%"

``` r
namevec <- map(database = "world", col = "blue",fill=T, namesonly=TRUE,plot=F)
map(database = "world",col = c("white", "forestgreen")[1+(namevec %in% df$region )],fill=T)
```

![](tweet_analysis_files/figure-markdown_github/unnamed-chunk-17-1.png)

For USA tweets, we can further highlight state locations in a map.

``` r
usa.data = rt.data[rt.data$country %in% 'United States',]
usa.data$state = as.character(lapply(strsplit(as.character(usa.data$place_full_name), 
                                              split=", "),tail, n=1))
usa.data = usa.data[nchar(usa.data$state)==2,]

namevec <- map(database = "state", col = "blue",fill=T, namesonly=TRUE,plot = F)
mapped.states = tolower(state.name[match(usa.data$state,state.abb)])
map(database = "state",col = c("white", "deepskyblue")[1+(namevec %in% mapped.states)],fill=T)
```

![](tweet_analysis_files/figure-markdown_github/unnamed-chunk-18-1.png)

Filter tweets
-------------

Now, we'll work with the full dataset rather than

``` r
textdata <- as.factor(rt.data$text)
textdata = gsub("[[:punct:]]", "", textdata)
textdata = gsub("[[:digit:]]", "", textdata)
textdata = gsub("http\\w+", "", textdata)
textdata = gsub("[ \t]{2,}", "", textdata)
textdata = gsub('\\p{So}|\\p{Cn}', "", textdata, perl = TRUE)
#textdata[textdata==" "] = NULL
#textdata = gsub("^\\s+|\\s+$", "", textdata)
try.error = function(x){
  y = NA
  try_error = tryCatch(tolower(x), error=function(e) e)
  if (!inherits(try_error, "error"))
    y = tolower(x)
  return(y)
}
textdata = sapply(textdata, try.error)
textdata = textdata[!is.na(textdata)]
names(textdata) = NULL
```

``` r
str(textdata)
```

    ##  chr [1:12451] "engagement book date unknown" ...

What's this data about?

``` r
tweetsCorpus <- Corpus(VectorSource(rt.data$text))
```

We can use a wordcloud to visualize our tweets. I used [this as source](https://www.r-bloggers.com/building-wordclouds-in-r/).

``` r
tweetsCorpus<- tm_map(tweetsCorpus, PlainTextDocument)
tweetsCorpus <- tm_map(tweetsCorpus, removePunctuation)
tweetsCorpus <- tm_map(tweetsCorpus, removeWords, stopwords('english'))
```

The previous will set as plain text, remove punctuation, and remove stopwords (e.g., I,if,but,etc.) from our tweets.

``` r
tweetsCorpus <- tm_map(tweetsCorpus, stemDocument)
```

Now get the root of the word by performing a stem process on tm map. For example, texting by text and classes by class

Wordcloud
---------

``` r
tweetsCorpus <- Corpus(VectorSource(tweetsCorpus))
wordcloud(tweetsCorpus, max.words = 100, random.order = FALSE)
```

![](tweet_analysis_files/figure-markdown_github/unnamed-chunk-24-1.png)

This method employ the R package tm.

``` r
wordcloud(textdata, max.words = 100, random.order = FALSE)
```

![](tweet_analysis_files/figure-markdown_github/unnamed-chunk-25-1.png)

While this wordcloud uses a manual text processing with gsub and regex.

For obvious reasons **UMass** is our top word. But we can also see tweets related to umass games (football and hockey).

Sentiment analysis
------------------

For the sentiment analysis, I used the R package SentimentAnalysis [based on this source](https://cran.r-project.org/web/packages/SentimentAnalysis/vignettes/SentimentAnalysis.html#dictionary-generation).

``` r
sentiment <- analyzeSentiment(textdata)
str(sentiment)
```

    ## 'data.frame':    12451 obs. of  14 variables:
    ##  $ WordCount         : num  4 3 6 12 8 7 9 14 6 12 ...
    ##  $ SentimentGI       : num  0.25 0.333 0.333 0 0.375 ...
    ##  $ NegativityGI      : num  0 0 0 0.0833 0 ...
    ##  $ PositivityGI      : num  0.25 0.3333 0.3333 0.0833 0.375 ...
    ##  $ SentimentHE       : num  0 0 0 0 0.125 0 0 0 0 0 ...
    ##  $ NegativityHE      : num  0 0 0 0 0 0 0 0 0 0 ...
    ##  $ PositivityHE      : num  0 0 0 0 0.125 0 0 0 0 0 ...
    ##  $ SentimentLM       : num  0 0 0.167 0 0.125 ...
    ##  $ NegativityLM      : num  0 0 0 0 0 ...
    ##  $ PositivityLM      : num  0 0 0.167 0 0.125 ...
    ##  $ RatioUncertaintyLM: num  0.25 0 0 0 0 0 0 0 0 0 ...
    ##  $ SentimentQDAP     : num  0 0.3333 0.1667 0.0833 0.125 ...
    ##  $ NegativityQDAP    : num  0.25 0 0 0.0833 0 ...
    ##  $ PositivityQDAP    : num  0.25 0.333 0.167 0.167 0.125 ...

Convert sentiment analysis to factors with directionality.

``` r
sentiment.directions <- convertToDirection(sentiment$SentimentGI)
sentiment.directions <- data.frame(emotion=sentiment.directions)
str(sentiment.directions)
```

    ## 'data.frame':    12451 obs. of  1 variable:
    ##  $ emotion: Factor w/ 3 levels "negative","neutral",..: 3 3 3 2 3 3 2 3 2 1 ...

``` r
ggplot(sentiment.directions, aes(x=emotion)) +
  geom_bar(aes(y=..count.., fill=emotion)) +
  scale_fill_brewer(palette="Dark2") +
  labs(x="emotion categories", y="")
```

![](tweet_analysis_files/figure-markdown_github/unnamed-chunk-28-1.png)

Convert sentiment analysis to factors with directionality.

``` r
sentiment.scale <- data.frame(sentiment.scale=sentiment$SentimentGI,polarity=convertToDirection(sentiment$SentimentGI))
str(sentiment.scale)
```

    ## 'data.frame':    12451 obs. of  2 variables:
    ##  $ sentiment.scale: num  0.25 0.333 0.333 0 0.375 ...
    ##  $ polarity       : Factor w/ 3 levels "negative","neutral",..: 3 3 3 2 3 3 2 3 2 1 ...

Assumption, NAs to be neutral

``` r
sentiment.scale$sentiment.scale[is.na(sentiment.scale$sentiment.scale)] = 0
```

``` r
ggplot(sentiment.scale[!sentiment.scale$sentiment.scale==0,], aes(x=sentiment.scale)) +
  geom_bar(aes(y=..count..,fill=polarity)) +
  labs(x="polarity categories", y="")
```

![](tweet_analysis_files/figure-markdown_github/unnamed-chunk-31-1.png)
